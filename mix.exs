defmodule Cache.MixProject do
  use Mix.Project

  def project do
    [
      app: :cache,
      version: "0.1.0",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      # Docs
      name: "Cache",
      source_url: "https://gitlab.com/yan.guiborat1/cache.git",
      docs: [
        # The main page in the docs
        main: "Cache",
        extras: ["README.md"]
      ],
      dialyzer: [flags: ["-Wunmatched_returns", :error_handling, :underspecs]]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.28.5"},
      {:dialyxir, "~> 1.2", only: [:dev], runtime: false}
    ]
  end
end
