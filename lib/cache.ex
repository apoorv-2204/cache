defmodule CacheEntry.StateData do
  @moduledoc """
  Struct describing the state of a cache entry FSM
  """

  defstruct running_func_task: :undefined,
            hydrating_func: :undefined,
            getters: [],
            ttl: :undefined,
            refresh_interval: :undefined,
            key: :undefined,
            value: :"$$undefined",
            timer_func: :undefined,
            timer_discard: :undefined
end

defmodule CacheEntry do
  @moduledoc """
  This module is a finite state machine implementing a cache entry.
  There is one such Cache Entry FSM running per registered key.

  It is responsible for :
  - receiving request to get the value for the key it is associated with
  - Run the hydrating function associated with this key
  - Manage various timers
  """
  @behaviour :gen_statem

  use Task
  require Logger

  @spec start_link :: :ignore | {:error, any} | {:ok, pid}
  def start_link do
    :gen_statem.start_link(__MODULE__, :ok, [])
  end

  @impl :gen_statem
  def init(_), do: {:ok, :idle, %CacheEntry.StateData{}}

  @impl :gen_statem
  def callback_mode, do: [:handle_event_function, :state_enter]

  @impl :gen_statem
  def handle_event(:cast, {:get, from}, :idle, data) do
    ## Value is requested while fsm is iddle, return the value
    send(from, {:ok, data.value})
    {:next_state, :idle, data}
  end

  def handle_event(:cast, {:get, from}, :running, data) when data.value == :"$$undefined" do
    ## Getting value when a function is running and no previous value is available
    ## Register this getter to send value later on
    previous_getters = data.getters
    {:next_state, :running, %CacheEntry.StateData{data | getters: previous_getters ++ [from]}}
  end

  def handle_event(:cast, {:get, from}, :running, data) do
    ## Getting value while function is running but previous value is available
    ## Return vurrent value
    send(from, {:ok, data.value})
    {:next_state, :running, data}
  end

  def handle_event({:call, from}, {:register, fun, key, ttl, refresh_interval}, :running, data) do
    ## Registering a new hydrating function while previous one is running
    ## We stop the task
    Task.shutdown(data.running_func_task, :brutal_kill)

    ## And the timers triggering it and discarding value
    _ = :timer.cancel(data.timer_func)
    _ = :timer.cancel(data.timer_discard)

    ## Start new timer to hydrate at refresh interval
    timer = :timer.send_interval(refresh_interval, self(), :hydrate)

    ## We trigger the update ( to trigger or not could be set at registering option )
    {:repeat_state,
     %CacheEntry.StateData{
       data
       | hydrating_func: fun,
         key: key,
         ttl: ttl,
         refresh_interval: refresh_interval,
         timer_func: timer
     }, [{:reply, from, :ok}]}
  end

  def handle_event({:call, from}, {:register, fun, key, ttl, refresh_interval}, _state, data) do
    ## Setting hydrating function in other cases
    ## Hydrating function not running, we just stop the timers
    _ = :timer.cancel(data.timer_func)
    _ = :timer.cancel(data.timer_discard)

    ## Fill state with new hydrating function parameters
    data =
      Map.merge(data, %{
        :hydrating_func => fun,
        :ttl => ttl,
        :refresh_interval => refresh_interval
      })

    timer = :timer.send_interval(refresh_interval, self(), :hydrate)

    ## We trigger the update ( to trigger or not could be set at registering option )
    {:next_state, :running,
     %CacheEntry.StateData{
       data
       | hydrating_func: fun,
         key: key,
         ttl: ttl,
         refresh_interval: refresh_interval,
         timer_func: timer
     }, [{:reply, from, :ok}]}
  end

  def handle_event(:info, :hydrate, :idle, data) do
    ## Time to rehydrate
    ## Hydrating the key, go to running state
    {:next_state, :running, data}
  end

  def handle_event(:enter, _event, :running, data) do
    ## At entering running state, we start the hydrating task
    me = self()

    hydrating_task =
      Task.Supervisor.async(Cache.TaskSupervisor, fn ->
        value = data.hydrating_func.()
        :gen_statem.cast(me, {:new_value, data.key, value})
      end)

    ## we stay in running state
    {:next_state, :running, %CacheEntry.StateData{data | running_func_task: hydrating_task}}
  end

  def handle_event(:info, :discarded, state, data) do
    ## Value is discarded

    Logger.warning(
      "Key :#{inspect(data.key)}, Hydrating func #{inspect(data.hydrating_func)} discarded"
    )

    {:next_state, state, %CacheEntry.StateData{data | value: :discarded}}
  end

  def handle_event(:cast, {:new_value, key, {:ok, value}}, :running, data) do
    ## We got result from hydrating function
    Logger.debug(
      "Key :#{inspect(data.key)}, Hydrating func #{inspect(data.hydrating_func)} got value #{inspect({key, value})}"
    )

    ## notify waiiting getters
    Enum.each(data.getters, fn getter -> send(getter, {:ok, value}) end)
    ## We could do error control here, like unregistering the running func.
    ## atm, we will keep it
    _ = :timer.cancel(data.timer_discard)
    me = self()
    {:ok, new_timer} = :timer.send_after(data.ttl, me, :discarded)

    {:next_state, :idle,
     %CacheEntry.StateData{
       data
       | running_func_task: :undefined,
         value: value,
         getters: [],
         timer_discard: new_timer
     }}
  end

  def handle_event(:cast, {:new_value, key, {:error, reason}}, :running, data) do
    ## Got error new value for key
    Logger.warning(
      "Key :#{inspect(data.key)}, Hydrating func #{inspect(data.hydrating_func)} got error value #{inspect({key, {:error, reason}})}"
    )

    {:next_state, :idle, %CacheEntry.StateData{data | running_func_task: :undefined}}
  end

  def handle_event(_type, _event, _state, data) do
    {:keep_state, data}
  end
end

defmodule Cache do
  @moduledoc """
  GenServer implementing the cache itself.
  It receives queries from clients requesting the cache, and manage the cache entries FSMs
  """
  use GenServer

  require Logger

  @name :cache

  @type result ::
          {:ok, any()}
          | {:error, :timeout}
          | {:error, :not_registered}

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, opts, name: @name)
  end

  @impl GenServer
  def init(_state) do
    _ =
      Supervisor.start_link(
        [
          {Task.Supervisor, name: Cache.TaskSupervisor}
        ],
        strategy: :one_for_one
      )

    {:ok, %{}}
  end

  @impl true
  def handle_call({:register, fun, key, ttl, refresh_interval}, _from, state) do
    ## Called when asked to register a function
    case Map.get(state, key) do
      nil ->
        ## New key, we start a cache entry
        {:ok, pid} = :gen_statem.start_link(CacheEntry, %CacheEntry.StateData{}, [])

        case :gen_statem.call(pid, {:register, fun, key, ttl, refresh_interval}) do
          :ok ->
            {:reply, :ok, Map.put(state, key, pid)}

          error ->
            {:error, error}
        end

      pid ->
        ## Key already exists, no need to start fsm
        ## TODO: Duplicate code :(
        case :gen_statem.call(pid, {:register, fun, key, ttl, refresh_interval}) do
          :ok ->
            {:reply, :ok, Map.put(state, key, pid)}

          error ->
            {:error, error}
        end
    end
  end

  def handle_call(unmanaged, _from, state) do
    Logger.warning("Cache received unmanaged call: #{inspect(unmanaged)}")
    {:reply, :ok, state}
  end

  @impl true
  def handle_cast({:get, from, key}, state) do
    case Map.get(state, key, :undefined) do
      :undefined ->
        send(from, {:error, :not_registered})
        {:noreply, state}

      pid ->
        :gen_statem.cast(pid, {:get, from})
        {:noreply, state}
    end
  end

  def handle_cast(unmanaged, state) do
    Logger.warning("Cache received unmanaged cast: #{inspect(unmanaged)}")
    {:noreply, state}
  end

  @impl true
  def handle_info(unmanaged, state) do
    Logger.warning("Cache received unmanaged info: #{inspect(unmanaged)}")
    {:noreply, state}
  end

  @doc ~s"""
  Registers a function that will be computed periodically to update the cache.

  Arguments:
    - `fun`: a 0-arity function that computes the value and returns either
      `{:ok, value}` or `{:error, reason}`.
    - `key`: associated with the function and is used to retrieve the stored
    value.
    - `ttl` ("time to live"): how long (in milliseconds) the value is stored
      before it is discarded if the value is not refreshed.
    - `refresh_interval`: how often (in milliseconds) the function is
      recomputed and the new value stored. `refresh_interval` must be strictly
      smaller than `ttl`. After the value is refreshed, the `ttl` counter is
      restarted.

  The value is stored only if `{:ok, value}` is returned by `fun`. If `{:error,
  reason}` is returned, the value is not stored and `fun` must be retried on
  the next run.
  """
  @spec register_function(
          fun :: (() -> {:ok, any()} | {:error, any()}),
          key :: any,
          ttl :: non_neg_integer(),
          refresh_interval :: non_neg_integer()
        ) :: :ok
  def register_function(fun, key, ttl, refresh_interval)
      when is_function(fun, 0) and is_integer(ttl) and ttl > 0 and
             is_integer(refresh_interval) and
             refresh_interval < ttl do
    GenServer.call(@name, {:register, fun, key, ttl, refresh_interval})
  end

  @doc ~s"""
  Get the value associated with `key`.

  Details:
    - If the value for `key` is stored in the cache, the value is returned
      immediately.
    - If a recomputation of the function is in progress, the last stored value
      is returned.
    - If the value for `key` is not stored in the cache but a computation of
      the function associated with this `key` is in progress, wait up to
      `timeout` milliseconds. If the value is computed within this interval,
      the value is returned. If the computation does not finish in this
      interval, `{:error, :timeout}` is returned.
    - If `key` is not associated with any function, return `{:error,
      :not_registered}`
  """
  @spec get(any(), non_neg_integer(), Keyword.t()) :: result
  def get(key, timeout \\ 30_000, _opts \\ [])
      when is_integer(timeout) and timeout > 0 do
    GenServer.cast(@name, {:get, self(), key})

    receive do
      {:ok, value} ->
        Logger.debug("Client received value #{inspect(value)}")
        {:ok, value}

      {:error, reason} ->
        Logger.error(
          "Error when getting value for key #{inspect(key)}   reason: #{inspect(reason)}"
        )

        {:error, reason}
    after
      timeout ->
        Logger.error("Client timed out waiting for result")
        {:error, :timeout}
    end
  end
end
