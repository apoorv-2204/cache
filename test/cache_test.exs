defmodule CacheTest do
  use ExUnit.Case
  doctest Cache
  require Logger

  test "If `key` is not associated with any function, return `{:error, :not_registered}`" do
    Cache.start_link()
    assert Cache.get("unexisting key") == {:error, :not_registered}
  end

  test "If value stored, it is returned immediatly" do
    Cache.start_link()

    result =
      Cache.register_function(
        fn ->
          {:ok, 1}
        end,
        "simple_func",
        50_000,
        10_000
      )

    assert result == :ok
    ## WAit a little to be sure value is registered and not being refreshed
    :timer.sleep(500)
    r = Cache.get("simple_func", 10_000)
    assert r == {:ok, 1}
  end

  test "Getting value for key while function is running first time make process wait and return value" do
    Cache.start_link()

    result =
      Cache.register_function(
        fn ->
          Logger.info("Hydrating function Sleeping 3 secs")
          :timer.sleep(3000)
          {:ok, 1}
        end,
        "test_long_function",
        50_000,
        9000
      )

    assert result == :ok

    r = Cache.get("test_long_function", 10_000)
    assert r == {:ok, 1}
  end

  test "Getting value for key while function is running first time returns timeout after ttl" do
    Cache.start_link()

    result =
      Cache.register_function(
        fn ->
          Logger.info("Hydrating function Sleeping 3 secs")
          :timer.sleep(3000)
          {:ok, 1}
        end,
        "test_get_ttl",
        50_000,
        9000
      )

    assert result == :ok

    ## get and wait up to 1 second
    r = Cache.get("test_get_ttl", 1000)
    assert r == {:error, :timeout}
  end

  test "Hydrating function runs periodically" do
    Cache.start_link()
    :persistent_term.put("test", 0)

    result =
      Cache.register_function(
        fn ->
          value = :persistent_term.get("test")
          value = value + 1
          :persistent_term.put("test", value)
          {:ok, value}
        end,
        "test_inc",
        50000,
        1000
      )

    assert result == :ok

    :timer.sleep(5000)
    {:ok, value} = Cache.get("test_inc", 3000)

    assert value >= 5
  end

  test "Update hydrating function while another one is running returns new hydrating value from new function" do
    Cache.start_link()

    result =
      Cache.register_function(
        fn ->
          :timer.sleep(5000)
          {:ok, 1}
        end,
        "test_reregister",
        50000,
        10000
      )

    assert result == :ok

    _result =
      Cache.register_function(
        fn ->
          {:ok, 2}
        end,
        "test_reregister",
        50000,
        10000
      )

    :timer.sleep(5000)
    {:ok, value} = Cache.get("test_reregister", 4000)

    assert value == 2
  end

  test "Getting value while function is running and previous value is available returns value" do
    Cache.start_link()

    _ =
      Cache.register_function(
        fn ->
          {:ok, 1}
        end,
        "test_reregister",
        50000,
        10000
      )

    _ =
      Cache.register_function(
        fn ->
          :timer.sleep(5000)
          {:ok, 2}
        end,
        "test_reregister",
        50000,
        10000
      )

    {:ok, value} = Cache.get("test_reregister", 4000)

    assert value == 1
  end

  test "Two hydrating function can run at same time" do
    Cache.start_link()

    _ =
      Cache.register_function(
        fn ->
          :timer.sleep(5000)
          {:ok, :result_timed}
        end,
        "timed_value",
        80000,
        70000
      )

    _ =
      Cache.register_function(
        fn ->
          {:ok, :result}
        end,
        "direct_value",
        80000,
        70000
      )

    ## We query the value with timeout smaller than timed function
    {:ok, _value} = Cache.get("direct_value", 2000)
  end
end
